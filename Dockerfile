# Builds the CLPI server
ARG SBCL_VERSION=2.1.2
FROM daewok/static-sbcl:$SBCL_VERSION-alpine3.13 as builder

ENV CLPM_SIGNING_KEY=10327DE761AB977333B1AD7629932AC49F3044CE
ENV CLPM_VERSION=0.4.0-alpha.3

# Install deps
# hadolint ignore=DL3018
RUN apk add --no-cache gnupg curl ca-certificates git gcc musl-dev linux-headers

# Install CLPM
# hadolint ignore=DL3003
RUN curl "https://files.clpm.dev/clpm/clpm-$CLPM_VERSION-linux-amd64.tar.gz" > "/tmp/clpm-$CLPM_VERSION-linux-amd64.tar.gz" \
    && curl "https://files.clpm.dev/clpm/clpm-$CLPM_VERSION.DIGESTS.asc" > "/tmp/clpm-$CLPM_VERSION.DIGESTS.asc" \
    && (gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys "$CLPM_SIGNING_KEY" || \
        gpg --batch --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys "$CLPM_SIGNING_KEY" || \
        gpg --batch --keyserver keyserver.ubuntu.com --recv-keys "$CLPM_SIGNING_KEY" || \
        gpg --batch --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys "$CLPM_SIGNING_KEY" || \
        gpg --batch --keyserver pgp.mit.edu --recv-keys "$CLPM_SIGNING_KEY") \
    && gpg --batch --verify "/tmp/clpm-$CLPM_VERSION.DIGESTS.asc" \
    && gpg --batch --decrypt "/tmp/clpm-$CLPM_VERSION.DIGESTS.asc" > "/tmp/clpm-$CLPM_VERSION.DIGESTS" \
    && grep clpm-$CLPM_VERSION-linux-amd64.tar.gz "/tmp/clpm-$CLPM_VERSION.DIGESTS" > /tmp/digests.txt \
    && (cd /tmp && sha512sum -c /tmp/digests.txt) \
    && (cd /tmp && tar xf "clpm-$CLPM_VERSION-linux-amd64.tar.gz" && cd "clpm-$CLPM_VERSION-linux-amd64" && sh install.sh) \
    && clpm version -V

WORKDIR /app
COPY clpi-server.asd clpi-server-asdf.asd clpmfile clpmfile.lock ./

# Install dependencies
RUN clpm bundle install --no-resolve -V

COPY clpi-server-asdf/ ./clpi-server-asdf/
COPY src/ ./src/
COPY scripts/ ./scripts/
COPY LICENSE README.org ./
COPY dep-licenses/ ./dep-licenses/

# build!
RUN clpm bundle exec -- sbcl --script scripts/build-release.lisp

# Unpack the tarball in an easy to locate place for the runtime image.
RUN mkdir -p /prefix \
    && tar xf /app/releases/dynamic/*.tar.gz -C /prefix --strip-components=1

FROM alpine:3.13

COPY --from=builder /prefix/bin/ /usr/local/bin/
COPY --from=builder /prefix/share/ /usr/local/share/

CMD ["/usr/local/bin/clpi-server"]
