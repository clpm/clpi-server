;;;; CLPI Server ASDF System Definition.
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(defsystem #:clpi-server-asdf
  :version "0.0.3"
  :license "BSD-2-Clause"

  :pathname "clpi-server-asdf"
  :depends-on (#:asdf-release-ops)
  :serial t
  :components ((:file "package")
               (:file "class")
               (:file "features")
               (:file "dep-licenses")))
