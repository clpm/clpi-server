;;;; ASDF Class
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(in-package #:clpi-server-asdf)

(defclass clpi-server-asdf-system (asdf-release-ops:package-inferred-release-system)
  ())
