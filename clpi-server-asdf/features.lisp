;;;; Features
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(in-package #:clpi-server-asdf)

(defmethod asdf-release-ops:program-image-features (o (s clpi-server-asdf-system))
  (list :hunchentoot-no-ssl))
