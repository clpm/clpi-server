;;;; CLPI Server System Definition.
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(asdf:load-system :asdf-release-ops)

(defsystem #:clpi-server
  :version "0.0.3"
  :license "BSD-2-Clause"

  :defsystem-depends-on (#:clpi-server-asdf)
  :class "clpi-server-asdf:clpi-server-asdf-system"

  :pathname "src"
  :entry-point "clpi-server:main"
  :depends-on (#:clpi-server/clpi-server)

  :release-staging-directory "../output/release-staging/"
  :release-directory "../releases/"

  :in-order-to ((asdf-release-ops:perform-program-image-op (load-op :hunchentoot)
                                                           (load-op :lack-middleware-accesslog)
                                                           (load-op :clack-handler-hunchentoot))))

(asdf:register-system-packages :bordeaux-threads '(#:bt))
(asdf:register-system-packages :lack-request '(#:lack.request))
(asdf:register-system-packages :lack-component '(#:lack.component))
