{{/*
Expand the name of the chart.
*/}}
{{- define "clpi-server.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "clpi-server.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "clpi-server.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "clpi-server.labels" -}}
helm.sh/chart: {{ include "clpi-server.chart" . }}
{{ include "clpi-server.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "clpi-server.selectorLabels" -}}
app.kubernetes.io/name: {{ include "clpi-server.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "clpi-server.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "clpi-server.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Return CLPI server password
*/}}
{{- define "clpi-server.password" -}}
{{- if not (empty .Values.global.clpiServer.password) }}
    {{- .Values.global.clpiServer.password -}}
{{- else if not (empty .Values.password) -}}
    {{- .Values.password -}}
{{- else -}}
    {{- randAlphaNum 16 -}}
{{- end -}}
{{- end -}}

{{/*
Return CLPI server username
*/}}
{{- define "clpi-server.username" -}}
{{- if not (empty .Values.global.clpiServer.username) }}
    {{- .Values.global.clpiServer.username -}}
{{- else if not (empty .Values.username) -}}
    {{- .Values.username -}}
{{- else -}}
    {{- printf "clpi" -}}
{{- end -}}
{{- end -}}

{{/*
Get the password secret.
*/}}
{{- define "clpi-server.secretName" -}}
{{- if .Values.existingSecret -}}
{{- printf "%s" .Values.existingSecret -}}
{{- else -}}
{{- printf "%s" (include "clpi-server.fullname" .) -}}
{{- end -}}
{{- end -}}

{{/*
Get the username key to be retrieved from secret.
*/}}
{{- define "clpi-server.secretUsernameKey" -}}
{{- if and .Values.existingSecret .Values.existingSecretUsernameKey -}}
{{- printf "%s" .Values.existingSecretUsernameKey -}}
{{- else -}}
{{- printf "clpi-server-username" -}}
{{- end -}}
{{- end -}}

{{/*
Get the password key to be retrieved from secret.
*/}}
{{- define "clpi-server.secretPasswordKey" -}}
{{- if and .Values.existingSecret .Values.existingSecretPasswordKey -}}
{{- printf "%s" .Values.existingSecretPasswordKey -}}
{{- else -}}
{{- printf "clpi-server-password" -}}
{{- end -}}
{{- end -}}

{{/*
Get the pvc name.
*/}}
{{- define "clpi-server.pvcName" -}}
{{- if .Values.persistence.existingClaim -}}
{{- printf "%s" .Values.persistence.existingClaim -}}
{{- else -}}
{{- printf "%s" (include "clpi-server.fullname" .) -}}
{{- end -}}
{{- end -}}