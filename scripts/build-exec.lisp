;;;; Build the CLPI server executable.
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(require :asdf)

(pushnew :hunchentoot-no-ssl *features*)

(defvar *build-dir* (merge-pathnames "../output/" (uiop:pathname-directory-pathname *load-pathname*)))

(asdf:load-system :clpi-server)
(asdf:load-system :hunchentoot)
(asdf:load-system :lack-middleware-accesslog)
(asdf:load-system :clack-handler-hunchentoot)

(defmethod asdf:output-files ((op asdf:program-op) (c (eql (asdf:find-system :clpi-server))))
  (values (list (merge-pathnames "clpi-server" *build-dir*)) t))

(defmethod asdf:operation-done-p ((op asdf:prepare-op) (c (eql (asdf:find-system :clpi-server))))
  nil)

(asdf:operate 'asdf:program-op :clpi-server)
(uiop:quit 0)
