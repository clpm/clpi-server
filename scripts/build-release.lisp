;;;; Build the CLPI server release.
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(require :asdf)

;; Should be unnecessary, but a lot of fukamachi-ware has package variance
;; issues and sometimes code gets loaded twice (a bug somewhere in
;; asdf-release-ops).
(asdf:load-system :clpi-server)

(asdf:operate 'asdf-release-ops:dynamic-release-archive-op :clpi-server)

(uiop:quit 0)
