;;;; CLPI Server API
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpi-server/api
    (:use #:cl
          #:alexandria
          #:clpi-server/auth
          #:clpi-server/conditions
          #:clpi-server/store)
  (:import-from #:babel
                #:octets-to-string)
  (:import-from #:bt)
  (:import-from #:clpi)
  (:import-from #:jsown)
  (:import-from #:lack.request
                #:request-content
                #:request-headers)
  (:import-from #:ningle
                #:*request*
                #:context)
  (:export #:add-api-routes))

(in-package #:clpi-server/api)

(defun find-or-make-project (project-name)
  (or (clpi:index-project (index) project-name nil)
      (let ((project (make-instance (clpi:index-project-class (index))
                                    :index (index)
                                    :name project-name
                                    :version-scheme :semver)))
        (setf (clpi:project-releases-object-version project) 0)
        (clpi:index-add-project (index) project)
        project)))

(defun find-or-make-system (system-name)
  (or (clpi:index-system (index) system-name nil)
      (let ((system (make-instance (clpi:index-system-class (index))
                                   :index (index)
                                   :name system-name)))
        (clpi:index-add-system (index) system)
        system)))

(defun get-request-jsown ()
  (let* ((content-vector (request-content *request*))
         (content-string (octets-to-string content-vector)))
    (jsown:parse content-string)))

(defun jsown-to-feature-expression (js)
  (if (listp js)
      (switch ((jsown:val js "type") :test #'equal)
        ("not"
         (list :not (jsown-to-feature-expression (jsown:val js "feature"))))
        ("and"
         (list :and (mapcar #'jsown-to-feature-expression (jsown:val js "features"))))
        ("or"
         (list :or (mapcar #'jsown-to-feature-expression (jsown:val js "features")))))
      (make-keyword (uiop:standard-case-symbol-name js))))

(defgeneric jsown-to-dep (js))

(defmethod jsown-to-dep ((js string))
  js)

(defmethod jsown-to-dep ((js list))
  (switch ((jsown:val js "type") :test #'equal)
    ("require"
     (list :require (jsown:val js "name")))
    ("version"
     (list :version (jsown:val js "name") (jsown:val js "version")))
    ("feature"
     (list :feature (jsown-to-feature-expression (jsown:val js "featureExpression"))
           (jsown-to-dep (jsown:val js "dependency"))))))

(defun add-release (project version js)
  (format t "Creating release ~S for ~S~%" version (clpi:project-name project))
  (let ((release (make-instance
                  (clpi:project-release-class project)
                  :project project
                  :version version
                  :archive-type (switch ((jsown:val js "archiveType") :test #'equal)
                                  ("tar.gz"
                                   :tar.gz)
                                  ("zip"
                                   :zip))
                  :url (jsown:val js "url")))
        (js-systems (jsown:val js "systems"))
        (system-file-ht (make-hash-table :test 'equal)))
    (dolist (js-system js-systems)
      (let* ((enough-namestring (jsown:val js-system "systemFile"))
             (system-name (jsown:val js-system "name"))
             (system (find-or-make-system system-name))
             (system-file (ensure-gethash enough-namestring system-file-ht
                                          (make-instance (clpi:index-system-file-class (index))
                                                         :release release
                                                         :enough-namestring enough-namestring)))
             (system-release
               (make-instance (clpi:index-system-release-class (index))
                              :name system-name
                              :system-file system-file
                              :dependencies (mapcar #'jsown-to-dep
                                                    (jsown:val-safe js-system "dependencies"))
                              :version (jsown:val-safe js-system "version")
                              :description (jsown:val-safe js-system "description")
                              :license (jsown:val-safe js-system "license"))))
        (clpi:system-add-system-release system system-release)
        (clpi:system-file-add-system-release system-file system-release)))
    ;; Add the system files to the release.
    (dolist (system-file (hash-table-values system-file-ht))
      (clpi:release-add-system-file release system-file))
    ;; Add the release to the project.
    (clpi:project-add-release project release))
  (clpi:index-save (index)))

(defun make-release (params)
  (with-login-required ()
    (bt:with-lock-held ((index-lock))
      (let* ((project-name (assoc-value params :project-name))
             (version (assoc-value params :version))
             (project (find-or-make-project project-name))
             (existing-release (clpi:project-release project version nil)))
        (if existing-release
            ;; We currently don't support overwriting releases... Error.
            '(409 nil nil)
            (progn
              (add-release project version (get-request-jsown))
              '(200 nil nil)))))))

(defun jsown-to-repo (js)
  (switch ((jsown:val js "type") :test 'equal)
    ("gitlab"
     (let ((host (jsown:val-safe js "host"))
           (path (jsown:val js "path")))
       (apply #'make-instance 'clpi:gitlab-repo :path path (when host (list :host host)))))))

(defun ensure-project-repo (params)
  (with-login-required ()
    (bt:with-lock-held ((index-lock))
      (let* ((project-name (assoc-value params :project-name))
             (project (find-or-make-project project-name))
             (jsown (get-request-jsown))
             (repo (jsown-to-repo jsown)))
        (if repo
            ;; Set the repo and save the index!
            (progn
              (setf (clpi:project-repo project) repo)
              (clpi:index-save (index))
              `(200 nil nil))
            ;; Otherwise there's a problem with the request.
            `(400 nil nil))))))

(defun add-api-routes (app)
  (psetf (ningle:route app "/api/v0.1/projects/:project-name/releases/:version" :method :put)
         'make-release
         (ningle:route app "/api/v0.1/projects/:project-name/repo" :method :put)
         'ensure-project-repo))
