;;;; CLPI Server App Defs
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpi-server/app-defs
    (:use #:cl)
  (:import-from #:bt)
  (:import-from #:lack.component
                #:call)
  (:import-from #:ningle
                #:context)
  (:export #:clpi-server
           #:clpi-server-index
           #:clpi-server-index-lock
           #:clpi-server-password
           #:clpi-server-username))

(in-package #:clpi-server/app-defs)

(defclass clpi-server (ningle:app)
  ((index
    :initarg :index
    :accessor clpi-server-index)
   (index-lock
    :initarg :index-lock
    :initform (bt:make-lock "index lock")
    :accessor clpi-server-index-lock)
   (username
    :initarg :username
    :reader clpi-server-username)
   (password
    :initarg :password
    :reader clpi-server-password)))

(defmethod call :before ((app clpi-server) env)
  (setf (context :clpi-index) (clpi-server-index app)
        (context :clpi-index-lock) (clpi-server-index-lock app)
        (context :clpi-app) app))
