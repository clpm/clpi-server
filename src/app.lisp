;;;; CLPI Server App
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpi-server/app
    (:use #:cl
          #:clpi-server/api
          #:clpi-server/app-defs
          #:clpi-server/clpi-file-handler
          #:clpi-server/conditions
          #:clpi-server/ui)
  (:import-from #:lack
                #:builder)
  (:export #:make-app))

(in-package #:clpi-server/app)

(defun make-app (index-root-dir username password)
  (let ((app (make-instance 'clpi-server
                            :index (make-instance 'clpi:index
                                                  :object-store
                                                  (make-instance 'clpi:file-object-store
                                                                 :root index-root-dir))
                            :username username
                            :password password)))
    (add-api-routes app)
    (add-ui-routes app)
    (builder :accesslog
             (make-clpi-server-condition-handler-middleware)
             (make-clpi-static-file-middleware :root-pathname index-root-dir :root-path "/clpi/v0.4/")
             app)))
