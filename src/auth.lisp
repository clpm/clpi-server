;;;; CLPI Server Auth
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpi-server/auth
    (:use #:cl
          #:clpi-server/app-defs
          #:clpi-server/conditions)
  (:import-from #:cl-base64
                #:base64-string-to-string)
  (:import-from #:lack.request
                #:request-headers)
  (:import-from #:ningle
                #:*request*
                #:context)
  (:import-from #:split-sequence
                #:split-sequence)
  (:export #:with-login-required))

(in-package #:clpi-server/auth)

(defun parse-authorization-header (authorization)
  (when (string= authorization "Basic " :end1 6)
    (let ((user-and-pass (base64-string-to-string (subseq authorization 6))))
      (split-sequence #\: user-and-pass))))

(defun call-with-login-required (thunk)
  (let ((authorization (gethash "authorization" (request-headers *request*))))
    (unless authorization
      (error 'simple-authorization-required :realm "restricted area"))
    (destructuring-bind (user &optional (pass ""))
        (parse-authorization-header authorization)
      (unless (and (equal user (clpi-server-username (context :clpi-app)))
                   (equal pass (clpi-server-password (context :clpi-app))))
        (error 'forbidden)))
    (funcall thunk)))

(defmacro with-login-required (() &body body)
  `(call-with-login-required (lambda () ,@body)))
