;;;; CLPI File Handler
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.
;;;;
;;;; Serves static files from a CLPI index.

(uiop:define-package #:clpi-server/clpi-file-handler
    (:use #:cl
          #:alexandria
          #:clpi-server/conditions)
  (:import-from #:local-time
                #:format-rfc1123-timestring
                #:universal-to-timestamp)
  (:export #:make-clpi-static-file-middleware))

(in-package #:clpi-server/clpi-file-handler)

(defun make-clpi-static-file-middleware (&key root-pathname root-path)
  (lambda (app)
    (lambda (env)
      (handle-file-request app env root-pathname root-path))))

(defun handle-file-request (app env root-pathname root-path)
  (let ((path-info (getf env :path-info)))
    (if (starts-with-subseq root-path path-info)
        (serve-file (subseq path-info (length root-path)) root-pathname)
        (funcall app env))))

(defun serve-file (enough-pathname defaults)
  (when (member :up (pathname-directory enough-pathname))
    (error 'simple-bad-request :format-control "Directory traversal is not allowed."))
  (when (uiop:absolute-pathname-p enough-pathname)
    (error 'simple-bad-request :format-control "Paths must be relative."))
  (let ((pn (merge-pathnames enough-pathname defaults)))
    (when (uiop:directory-pathname-p pn)
      (error 'simple-bad-request :format-control "Cannot handle directories (yet)"))
    (unless (probe-file pn)
      (error 'simple-not-found))
    `(200
      (:content-type "text/plain; charset=utf-8"
       :last-modified ,(format-rfc1123-timestring nil (universal-to-timestamp (file-write-date pn))))
      ,pn)))
