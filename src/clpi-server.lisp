;;;; CLPI Server
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpi-server/clpi-server
    (:nicknames #:clpi-server)
  (:use #:cl
        #:clpi-server/app
        #:clpi-server/entry)
  (:reexport #:clpi-server/entry))

(in-package #:clpi-server)
