;;;; CLPI Server Conditions
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpi-server/conditions
    (:use #:cl)
  (:export #:authorization-required
           #:bad-request
           #:clpi-condition
           #:clpi-condition-code
           #:clpi-condition-headers
           #:clpi-condition-body
           #:forbidden
           #:make-clpi-server-condition-handler-middleware
           #:not-found
           #:simple-authorization-required
           #:simple-bad-request
           #:simple-forbidden
           #:simple-not-found))

(in-package #:clpi-server/conditions)

(define-condition clpi-condition ()
  ((content-type
    :initform "text/plain"
    :initarg :content-type
    :reader clpi-condition-content-type)))

(define-condition simple-clpi-condition (clpi-condition simple-condition)
  ())

(defgeneric clpi-condition-code (c))

(defgeneric clpi-condition-headers (c))

(defgeneric clpi-condition-body (c))

(defgeneric clpi-condition-standard-body (c))

(defmethod clpi-condition-headers ((c clpi-condition))
  (list :content-type (clpi-condition-content-type c)))

(defmethod clpi-condition-body ((c clpi-condition))
  (list (clpi-condition-standard-body c)))

(defmethod clpi-condition-body ((c simple-clpi-condition))
  (let ((format-control (simple-condition-format-control c))
        (format-args (simple-condition-format-arguments c)))
    (if format-control
        (list (apply #'format nil format-control format-args))
        (call-next-method))))


;; Generic 400

(define-condition bad-request (clpi-condition)
  ())

(define-condition simple-bad-request (simple-clpi-condition bad-request)
  ())

(defmethod clpi-condition-code ((c bad-request))
  400)

(defmethod clpi-condition-standard-body ((c bad-request))
  "Bad request")


;; 401

(define-condition authorization-required (clpi-condition)
  ((realm
    :initform nil
    :initarg :realm
    :reader authorization-required-realm)))

(define-condition simple-authorization-required (simple-clpi-condition authorization-required)
  ())

(defmethod clpi-condition-code ((c authorization-required))
  401)

(defmethod clpi-condition-headers ((c authorization-required))
  (list* :www-authenticate (format nil "Basic realm=~A" (authorization-required-realm c))
         (when (next-method-p)
           (call-next-method))))

(defmethod clpi-condition-standard-body ((c authorization-required))
  "Authorization required")


;; 403

(define-condition forbidden (clpi-condition)
  ())

(define-condition simple-forbidden (simple-clpi-condition forbidden)
  ())

(defmethod clpi-condition-code ((c forbidden))
  403)

(defmethod clpi-condition-standard-body ((c forbidden))
  "Forbidden")


;; 404

(define-condition not-found (clpi-condition)
  ())

(define-condition simple-not-found (simple-clpi-condition not-found)
  ())

(defmethod clpi-condition-code ((c not-found))
  404)

(defmethod clpi-condition-standard-body ((c not-found))
  "Not found")


;; Middleware

(defun make-clpi-server-condition-handler-middleware ()
  (lambda (app)
    (lambda (env)
      (handler-case
          (funcall app env)
        (clpi-condition (c)
          (list (clpi-condition-code c)
                (clpi-condition-headers c)
                (clpi-condition-body c)))))))
