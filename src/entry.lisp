;;;; CLPI Server Entrypoint
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpi-server/entry
    (:use #:cl
          #:alexandria
          #:clpi-server/app)
  (:import-from #:clack
                #:clackup)
  (:export #:main))

(in-package #:clpi-server/entry)

(defun main ()
  (let ((root (uiop:getenv "CLPI_SERVER_ROOT"))
        (username (uiop:getenv "CLPI_SERVER_USERNAME"))
        (password (uiop:getenv "CLPI_SERVER_PASSWORD"))
        (port (or (uiop:getenv "CLPI_SERVER_PORT") 5000))
        (address (or (uiop:getenv "CLPI_SERVER_ADDRESS") "127.0.0.1")))
    (unless (and root username password)
      (error "All env vars not set!"))
    (setf root (uiop:ensure-directory-pathname root))
    (when (stringp port)
      (setf port (parse-integer port)))
    ;; Wait for the index file to be present.
    (loop
      :until (probe-file (merge-pathnames "clpi-version" root))
      :do
         (format *error-output* "Waiting for clpi-version to be present ~%")
         (sleep 10))
    (clackup (make-app root username password)
             :use-thread nil :port port
             :address address)))
