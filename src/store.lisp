;;;; CLPI Server Store
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpi-server/store
    (:use #:cl)
  (:import-from #:ningle
                #:context)
  (:export #:index
           #:index-lock))

(in-package #:clpi-server/store)

(defun index ()
  (context :clpi-index))

(defun index-lock ()
  (context :clpi-index-lock))
