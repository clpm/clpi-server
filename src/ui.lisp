;;;; CLPI Server UI
;;;;
;;;; This software is part of clpi-server. See README.org for more
;;;; information. See LICENSE for license information.

(uiop:define-package #:clpi-server/ui
    (:use #:cl
          #:alexandria
          #:clpi-server/auth
          #:clpi-server/conditions
          #:clpi-server/store)
  (:import-from #:bt)
  (:import-from #:clpi)
  (:import-from #:ningle)
  (:import-from #:spinneret)
  (:export #:add-ui-routes))

(in-package #:clpi-server/ui)

(defmacro with-page ((&key title) &body body)
  `(spinneret:with-html-string
     (:doctype)
     (:html
      (:head
       (:title ,title))
      (:body ,@body))))

(defun projects (params)
  (declare (ignore params))
  (bt:with-lock-held ((index-lock))
    (let ((projects (sort (clpi:index-projects (index)) #'string< :key 'clpi:project-name)))
      `(200
        (:content-type "text/html")
        (,(with-page (:title "Projects")
            (:header
             (:h1 "Projects"))
            (dolist (project projects)
              (:li
               (:a :href (format nil "/projects/~A" (clpi:project-name project))
                   (clpi:project-name project))))))))))

(defun get-project (params)
  (bt:with-lock-held ((index-lock))
    (let ((project (clpi:index-project (index) (assoc-value params :project-name) nil)))
      (unless project
        (error 'not-found))
      (let ((repo (clpi:project-repo project)))
        `(200
          (:content-type "text/html")
          (,(with-page (:title (clpi:project-name project))
              (:header
               (:h1 (clpi:project-name project)))
              (when repo
                (:section
                 (:h2 "Repo")
                 (let ((*print-case* :downcase))
                   (format nil "~S" (clpi:repo-to-description repo)))))
              (when (clpi:project-releases project)
                (:section
                 (:h2 "Releases")
                 (:ul (dolist (release (clpi:project-releases project))
                        (:li (clpi:release-version release)
                             (:ul (dolist (system (sort (clpi:release-system-names release)
                                                        #'string<))
                                    (:li system)))))))))))))))

(defun home-page (params)
  (declare (ignore params))
  `(200
    (:content-type "text/html")
    (,(with-page (:title "CLPI")
        (:header
         (:h1 "CLPI"))
        (:section
         (:a :href "/projects" "Projects"))))))

(defun add-ui-routes (app)
  (setf (ningle:route app "/" :method :get)
        'home-page)
  (setf (ningle:route app "/projects" :method :get)
        'projects)
  (setf (ningle:route app "/projects/" :method :get)
        'projects)
  (setf (ningle:route app "/projects/:project-name" :method :get)
        'get-project))
